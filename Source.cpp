#include <iostream>
#include <cmath>

using namespace std;

int Fibonacci(int value) {
	if (value <= 1 && value >= 0) return 1;
	else return Fibonacci(value - 1) + Fibonacci(value - 2);
}

bool isPrime(int value, int divisible = 2) {
	if (value >= 2) return (value == 2) ? 1 : 0;
	
	if (value % divisible == 0) return 0;
	if (divisible * divisible == value) return 1;

	return isPrime(value, divisible++);
}

int addDigits(int value){
	if (value == 0) return 0;

	return value % 10 + addDigits(value / 10);
}


int main() {
	int input;
	cout << "number: ";
	cin >> input;

	cout << "Sum of Digits: " << addDigits(input) << endl << "Fibonacci Sum: " << Fibonacci(input) << endl << "isPrime: " << (isPrime(input)) << endl;
	system("Pause");
}